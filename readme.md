# Magento Patch Tool

This is a simple tool which helps you to patch a magento instance.

### Installation

* Clone Repo
```sh
$ git clone [git-repo-url] [dir-name] && cd [dir-name]
```
* Download the patches you want to apply to the system and copy them into the directory
```sh
 [dir-name]
```
* Run the patch tool and follow the onscreen instructions
```sh
$ sh patchTool.sh
```

### Note
This script DOES

* backup the webroot if you specify it during runtime
* backup the database if you specify it during runtime
* copy a patchfile to the remote server if you specify it during runtime

This script DOES NOT

* install the patch for you, you will need to connect to the remote server after the patch has been run and execute the patch. The script will make the script executable during runtime on the 
remote system so you will only need to run the command
```sh
$ sh [PATCHFILE].sh
```

### Todo's

Make this script failsafe!