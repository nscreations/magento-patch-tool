#!/bin/bash
clear
SWITCH="\033["
NORMAL="${SWITCH}0m"
RED="${SWITCH}1;31m"
GREEN="${SWITCH}1;32m"
CYAN="${SWITCH}1;36m"
YELLOW="${SWITCH}1;33m"
OS=`uname`
ARGUMENT1=$1
ARGUMENT2=$2

echo "${RED}Welcome to the magento patch tool!"
echo ""
echo "This script will backup a magento installation incl. DB and copy the patchfile onto the system, so you will only need to execute the patch afterwards"
echo "Download Patches here: https://www.magentocommerce.com/download"
echo "Make sure you check the Patch dependencies, some Patches require another Patch to be applied first!"
echo ""
echo "Author: N. Simmler <n.simmler@insign.ch>"
echo "Version: 1578beta2${NORMAL}" #Versioncode: YYMD<status<increment>>
echo ""

if [[ ${ARGUMENT1} == 'help' || ${ARGUMENT1} == 'h' || ${ARGUMENT1} == '?' ]]; then
    echo "
    usage : sh $0 help|only-db|only-webroot|only-copy|version <versionname>

    help                    Displays this message
    only-db                 Does only DB Backup
    only-webroot            Does only Webroot Backup
    only-copy               Does only copy the patchfile to the remote system
    version <versionname>   Does all the work in a version subdirectory (not compatible with the only-* options)
    "
    exit 1
fi

if [[ ${ARGUMENT1} == 'onlydb' ]]; then
    DOWEBROOT="N"
    DODATABASE="Y"
    DOCOPY="N"
fi

if [[ ${ARGUMENT1} == 'onlywebroot' ]]; then
    DOWEBROOT="Y"
    DODATABASE="N"
    DOCOPY="N"
fi

if [[ ${ARGUMENT1} == 'onlycopy' ]]; then
    DOWEBROOT="N"
    DODATABASE="N"
    DOCOPY="Y"
fi

if [[ -z ${ARGUMENT1} ]]; then
    echo "${YELLOW}Do you want to backup the webroot? [Y/N]${NORMAL}"
    read DOWEBROOT
    echo "${YELLOW}Do you want to backup the database? [Y/N]${NORMAL}"
    read DODATABASE
    echo "${YELLOW}Do you want to copy the patchfile? [Y/N]${NORMAL}"
    read DOCOPY
fi

if [[ ${DOWEBROOT} == 'y' || ${DOWEBROOT} == 'Y' ]] || [[ ${DODATABASE} == 'y' || ${DODATABASE} == 'Y' ]]; then
    if [[ ${ARGUMENT1} == 'version' && ! -z ${ARGUMENT2} ]]; then
        mkdir ${ARGUMENT2}
        VERSION=${ARGUMENT2}
    else
        VERSION=''
    fi
    #Set Backup Folder
    echo "${GREEN}Please specify the folder in which we will backup our files:${NORMAL}"
    read BACKUPDIR
    if [[ ${ARGUMENT1} == 'version' && ! -z ${ARGUMENT2} ]]; then
        BACKUPDIR="${VERSION}/${BACKUPDIR}"
    fi
    if [ -d "${BACKUPDIR}" ]; then
        echo "Directory ${BACKUPDIR} exists, proceeding"
        echo "Flushing database folder"
        rm ${BACKUPDIR}/database/*
    else
        echo "The directory doesn't exist, I will create one for you"
        mkdir "${BACKUPDIR}"
    fi
    ## Populate Backup Folders
    echo "Check if backup folder is populated"
    mkdir -p "${BACKUPDIR}"/"webroot" "${BACKUPDIR}"/"database"
fi

#Get SSH Credentials
echo "${GREEN}Which server are we working on (i.e. user@server.ch)? ${NORMAL}"
read SSHSERVER

ssh -q "${SSHSERVER}" exit
if [ $? -ne 0 ]; then
    echo "${RED}Couldn't connect via ssh, please make sure to add working access data or check the permission on the webserver${NORMAL}"
    exit 1
else
    echo "SSH Connection succeeded, proceeding"
fi

#Get Server Webroot
echo "${GREEN}Magento webroot (i.e. wwwdev or wwwtest or www, note some server have something like www/shop): ${NORMAL}"
read SERVERWEBROOT

if [[ ${DOWEBROOT} == 'y' || ${DOWEBROOT} == 'Y' ]]; then
    #Backup Webroot
    echo "Backing up webroot"
    rsync -rvq --ignore-existing -e ssh --exclude-from 'exclude-list-for-backup.txt' ${SSHSERVER}:${SERVERWEBROOT} ${BACKUPDIR}/webroot/
    echo "Backup done"
fi

if [[ ${DODATABASE} == 'y' || ${DODATABASE} == 'Y' ]]; then
    #Get DB Credentials
    echo "Reading DB Credentials from remote"
    rsync -q -e ssh ${SSHSERVER}:${SERVERWEBROOT}/app/etc/local.xml ${BACKUPDIR}/local.xml
    if [ -f "${BACKUPDIR}/local.xml" ]; then
        data=`cat ${BACKUPDIR}/local.xml`
        REMOTEDBNAME=$(perl -ne "print and last if s/.*<dbname>(.*)<\/dbname>.*/\1/;" <<< "$data")
        if [[ "${OS}" == 'Linux' ]]; then
            REMOTEDBNAME=${LOCALDBNAME:9:-3}
        else
            REMOTEDBNAME=${LOCALDBNAME#?????????}
            REMOTEDBNAME=${LOCALDBNAME%???}
        fi
        REMOTEDBUSER=$(perl -ne "print and last if s/.*<username>(.*)<\/username>.*/\1/;" <<< "$data")
        if [[ "${OS}" == 'Linux' ]]; then
            REMOTEDBUSER=${LOCALDBUSER:9:-3}
        else
            REMOTEDBUSER=${LOCALDBUSER#?????????}
            REMOTEDBUSER=${LOCALDBUSER%???}
        fi
        REMOTEDBPW=$(perl -ne "print and last if s/.*<password>(.*)<\/password>.*/\1/;" <<< "$data")
        if [[ "${OS}" == 'Linux' ]]; then
            REMOTEDBPW=${LOCALDBPW:9:-3}
        else
            REMOTEDBPW=${LOCALDBPW#?????????}
            REMOTEDBPW=${LOCALDBPW%???}
        fi

    else
      echo "${RED}Cannot read DB credentials (local.xml doesn't exist), aborting${NORMAL}"
      exit 1
    fi
    rm "${BACKUPDIR}/local.xml"

    #Backup DB
    echo "Backing up DB"
    ssh "${SSHSERVER}" "mysqldump -h localhost -u ${REMOTEDBUSER} -p${REMOTEDBPW} ${REMOTEDBNAME} | gzip -cf" | gunzip -c > "${BACKUPDIR}"/database/database.sql
    echo "Backup of DB done"
fi

echo "Retrieving already applied patches from system..."
echo "${CYAN}"
ssh "${SSHSERVER}" "cat ${SERVERWEBROOT}/app/etc/applied.patches.list"
echo "${NORMAL}"

if [[ ${DOCOPY} == 'y' || ${DOCOPY} == 'Y' ]]; then
    #Copy Patch
    echo "${GREEN}Specify the patchfile to be applied on the system (patchfile has to be in this directory and we want the full filename):${NORMAL} "
    read PATCHFILE
    echo "Copying patchfile"
    rsync --progress -e ssh ${PATCHFILE} ${SSHSERVER}:${SERVERWEBROOT}/${PATCHFILE}
    echo "Copyjob done"

    #Make Patch executable
    ssh "${SSHSERVER}" "chmod +x ${SERVERWEBROOT}/${PATCHFILE}"
else
    PATCHFILE="<patchfile>"
fi

echo "${RED}"
echo "You should now ssh to the system '${SSHSERVER}', cd into '${SERVERWEBROOT}' and apply the patch with the command 'sh ${PATCHFILE}' and delete the file afterwards!"
echo "You should also consider uploading the patch to our git repo of this webproject!"
echo "${NORMAL}"
exit 0
